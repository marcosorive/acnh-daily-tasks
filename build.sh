echo "---- Creating dist folder ----"
mkdir -p dist/public
echo "--- Building server ----"
cd server && npm i && npm run build-dist
echo "--- Copying package.json and dist ----"
cp package*.json ../dist
cp -r dist/* ../dist
cp -r src/config ../dist
echo "---- Building client ----"
cd ../client && npm i && npm run build
echo "---- Copying client ----"
cp -r dist/acnh-daily-tasks/** ../dist/public
echo "---- Done! ----"