import { Request, Response, NextFunction } from "express";

export class AuthService {

    constructor() { }

    public isAuthorized (req: Request, res: Response, next: NextFunction): void {
        if(req.cookies.user === undefined){
            res.status(401).send();
        }else{
            next();
        }
    }
}