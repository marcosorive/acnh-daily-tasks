import { ITaskRepository } from '../repositories/task/ITaskRepository';
import { ITask } from '../interfaces/ITask';
import { Bootstrap } from '../Bootstrap';
import { Task } from '../models/Task';

export class TaskService{

    constructor(private taskRepository: ITaskRepository){}

    // public getAllTaskFromUser(userCookieId: string): Promise<ITask[]>{
    //     try {
    //         return this.taskRepository.getAll();
    //     } catch (error) {
    //         Bootstrap.logger.log("error","Error in TaskService.getAllTasks: " + error);
    //         throw new Error(error);
    //     }
    // }

    public async getAll(): Promise<ITask[]>{
        try{ 
            return await this.taskRepository.getAll();
        }
        catch(error){
            Bootstrap.logger.log("error","Error in TaskService.addDefaultTasksToUser: " + error);
            throw new Error(error);
        }
    }

    public getDefaultTasks(){
        try{ 
            return this.taskRepository.getDefaultTasks();
        }
        catch(error){
            Bootstrap.logger.log("error","Error in TaskService.getDefaultTasks: " + error);
            throw new Error(error);
        }
    }

    public async getTask(id: String): Promise<ITask>{
        try{
            return await this.taskRepository.getTask(id);
        }
        catch(error){
            Bootstrap.logger.log("error","Error in TaskService.getTask: " + error);
            throw new Error(error);
        }
    }

    public async addTask(requestTask: any): Promise<ITask>{
        try {
            const Task = this.getTaskFromRequestTask(requestTask);
            return await this.taskRepository.addTask(Task);
        } catch (error) {
            Bootstrap.logger.log("error","Error in TaskService.addTask: " + error);
            throw new Error(error);
        }
    }
    
    public async updateTask(id:string, requestTask: any): Promise<ITask>{
        try {
            const task = this.getTaskFromRequestTask(requestTask);
            return await this.taskRepository.updateTask(id,task);
        } catch (error) {
            Bootstrap.logger.log("error","Error in TaskService.updateTask: " + error);
            throw new Error(error);
        }
    }

    public async deleteTask(id:string): Promise<boolean>{
        try {
            return await this.taskRepository.deleteTask(id);
        } catch (error) {
            Bootstrap.logger.log("error","Error in TaskService.deleteTask: " + error);
            throw new Error(error);
        }
    }

    private getTaskFromRequestTask(requestTask: any): ITask{
        let task: ITask = new Task();
        task.name = requestTask.name;
        return task;
    }
    
    private stringToSlug (str: String): String {
        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();
      
        // remove accents, swap ñ for n, etc
        var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
        var to   = "aaaaeeeeiiiioooouuuunc------";
        for (var i=0, l=from.length ; i<l ; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }
    
        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes
    
        return str;
    }
}