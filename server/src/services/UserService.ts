import { v4 as uuidv4 } from "uuid";
import { ITask } from "../interfaces/ITask";
import { IUser } from "../interfaces/IUser";
import { IUserRepository } from "../repositories/user/IUserRepository";
import { TaskService } from "./TaskService";
import { Bootstrap } from '../Bootstrap'

export class UserService {

    constructor(private userRepository: IUserRepository, private taskService: TaskService) { }

    public getAllTaskFromUser(userCookieId: string): Promise<ITask[]>{
        try {
            return this.userRepository.getTasksFromUser(userCookieId);
        } catch (error) {
            Bootstrap.logger.log("error","Error in UserService.getAllTaskFromUser: " + error);
            throw new Error(error);
        }
    }

    public async createUser(): Promise<IUser>{
        try {
            const cookieId: string = uuidv4();
            const tasks: ITask[] = await this.taskService.getDefaultTasks();
            let user: IUser = await this.userRepository.createUser(cookieId);
            return await this.userRepository.replaceTasksInUser(user, tasks);
        } catch (error) {
            Bootstrap.logger.log("error","Error in UserService.createUser: " + error);
            throw new Error(error);
        }
    }

    public async createTask(cookieId: string, taskName: string ): Promise<ITask>{
        try {
            if(taskName === "" || taskName === " "){
                throw new Error("Task name can't be empty")
            }
            return await this.userRepository.addTaskToUser(cookieId, taskName);
        } catch (error) {
            Bootstrap.logger.log("error","Error in UserService.createUser: " + error);
            throw new Error(error);
        }
    }

    public async deleteTask(cookieId: string, taskId: string ): Promise<boolean>{
        try {
            return await this.userRepository.deleteTaskFromUser(cookieId, taskId);
        } catch (error) {
            Bootstrap.logger.log("error","Error in UserService.createUser: " + error);
            throw new Error(error);
        }
    }

    public async updateTask(cookieId: string, taskId: string, taskName: string, taskStatus: string): Promise<ITask>{
        try {
            return await this.userRepository.updateTaskFromUser(cookieId, taskId, taskName, this.parseBoolean(taskStatus));
        } catch (error) {
            Bootstrap.logger.log("error","Error in UserService.createUser: " + error);
            throw new Error(error);
        }
    }

    public async markAllTasksUndone(cookieId: string): Promise<ITask[]>{
        try {
            return await this.userRepository.markAllUserTasksAsUndone(cookieId);
        } catch (error) {
            Bootstrap.logger.log("error","Error in UserService.createUser: " + error);
            throw new Error(error);
        }
    }


    private parseBoolean(status: string){
        return status.toString() === "true";
    }

}