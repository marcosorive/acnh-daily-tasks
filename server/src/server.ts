"use strict";
import { App } from "./app";

try{
    const PORT = 5000;
    new App(PORT).startApp();
}catch(error){
    console.log("Error in server.js: " + error);
}

