import { Logger } from 'winston';
import { TaskRoutes } from './routes/TaskRoutes';
import { TaskService } from "./services/TaskService";
import { TaskRepository } from './repositories/task/TaskRepository';
import { Task } from './models/Task';
import { UserRepository } from "./repositories/user/UserRepository";
import { UserService } from "./services/UserService";
import { User } from "./models/User";
import { AuthService } from "./services/AuthService";
import { logger } from './utils/logger';
import { TaskController } from './controllers/TaskController';

export class Bootstrap{
    public static taskRoutes: TaskRoutes;
    public static taskService: TaskService;
    // public static authService: AuthService;
    // public static userRepository: UserRepository;
    public static userService: UserService;
    public static logger: Logger;

    constructor(){}

    public setup(): void{
        const taskRepository = new TaskRepository(Task);
        const userRepository = new UserRepository(User);
        const taskService = new TaskService(taskRepository);
        const userService = new UserService(userRepository, taskService);
        const authService = new AuthService();
        const taskController = new TaskController();
        const taskRoutes = new TaskRoutes(authService, taskController);

        Bootstrap.userService = userService;
        Bootstrap.taskService = taskService
        Bootstrap.taskRoutes = taskRoutes;
        Bootstrap.logger = logger;
    }
}