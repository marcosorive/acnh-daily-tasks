import { IUser } from "../../interfaces/IUser";
import { ITask } from "../../interfaces/ITask";

export interface IUserRepository{
    createUser(cookieId: string): Promise<IUser>;
    getTasksFromUser(userCookieId: string): Promise<ITask[]>;
    replaceTasksInUser(user: IUser, tasks: ITask[]): Promise<IUser>;
    addTaskToUser(cookieId: string, taskName: string): Promise<ITask>;
    deleteTaskFromUser(cookieId: string, taskId: string): Promise<boolean>;
    updateTaskFromUser(cookieId: string, taskId: string, taskName: string, taskStatus: boolean): Promise<ITask>;
    markAllUserTasksAsUndone(cookieId: string): Promise<ITask[]>
}