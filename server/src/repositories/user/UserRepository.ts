import { IUserRepository } from "./IUserRepository";
import { IUser } from "../../interfaces/IUser";
import { ITask } from "../../interfaces/ITask";
import { Model } from "mongoose";
import { Bootstrap } from "../../Bootstrap";
import { Task} from "../../models/Task";

export class UserRepository implements IUserRepository{

    constructor(private User: Model<IUser>){}


    public async createUser(cookieId: string): Promise<IUser>{
        try {
            const user: IUser = await new this.User({ cookieId });
            return user;
        } catch (error) {
            Bootstrap.logger.log("error","Error in UserRepository.createUser: " + error);
            throw new Error(error);
        }
    }

    public async getTasksFromUser(cookieId: string): Promise<ITask[]> {
        try {
            const user: IUser = await this.User.findOne({ cookieId });
            return user.taskList;
        } catch (error) {
            Bootstrap.logger.log("error","Error in UserRepository.getTasksFromUser: " + error);
            throw new Error(error);
        }
    }

    public async replaceTasksInUser(user: IUser, tasks: ITask[]): Promise<IUser>{
        try {
            user.taskList = tasks;
            return user.save();
        } catch (error) {
            Bootstrap.logger.log("error","Error in UserRepository.getTasksFromUser: " + error);
            throw new Error(error);
        }
    }

    public async addTaskToUser(cookieId: string, taskName: string): Promise<ITask>{
        try {
            const user: IUser = await this.User.findOne({cookieId});
            let task: ITask = new Task();
            task.name = taskName;
            user.taskList.push(task);
            user.save();
            return Promise.resolve(task);
        } catch (error) {
            Bootstrap.logger.log("error","Error in UserRepository.addTaskToUser: " + error);
            throw new Error(error);
        }
    }

    public async deleteTaskFromUser(cookieId: string, taskId: string): Promise<boolean>{
        try {
            const user: IUser = await this.User.findOne({cookieId});
            var deleted: boolean = false;
            user.taskList = user.taskList.filter((task) => {
                if(task._id.toString() === taskId){
                    deleted = true;
                    return false;
                }
                return true;
            })
            user.save();
            return Promise.resolve(deleted);
        } catch (error) {
            Bootstrap.logger.log("error","Error in UserRepository.deleteTaskFromUser: " + error);
            throw new Error(error);
        }
    }

    public async updateTaskFromUser(cookieId: string, taskId: string, taskName: string, taskStatus: boolean): Promise<ITask>{
        try {
            const user: IUser = await this.User.findOne({cookieId});
            var selectedTask: ITask = undefined;
            user.taskList = user.taskList.map((task) => {
                if(task._id.toString() === taskId){
                    task.name = taskName || task.name;
                    task.isDone = taskStatus;
                    selectedTask = task;
                    return task;
                }
                return task;
            })
            if(selectedTask){
                user.save();
                return Promise.resolve(selectedTask);
            }
            else{
                return Promise.resolve(undefined);
            }            
        } catch (error) {
            Bootstrap.logger.log("error","Error in UserRepository.updateTaskFromUser: " + error);
            throw new Error(error);
        }
    }

    public async markAllUserTasksAsUndone(cookieId: string): Promise<ITask[]>{
        try {
            const user: IUser = await this.User.findOne({cookieId});
            user.taskList = user.taskList.map((task) => {
                task.isDone = false;
                return task;
            })
            return (await user.save()).taskList;
        } catch (error) {
            Bootstrap.logger.log("error","Error in UserRepository.markUserTasksAsUndone: " + error);
            throw new Error(error);
        }
    }
     
}