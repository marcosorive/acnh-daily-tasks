export const DEFAULT_TASKS: string[] = [
 "Talk to all your neighbors",
 "Check the Nook Stop inside Resident Services",
 "Check the recycle inside Resident Services",
 "Shake every tree in the island",
 "Pick up items on the beach",
 "Hit every rock in the island",
 "Plant a money tree",
 "Dig up every fossil in the island",
 "Visit every shop",
 "Look for island visitors"
]