"use strict";
import { ITaskRepository } from './ITaskRepository';
import { ITask } from '../../interfaces/ITask'
import { Model } from 'mongoose';
import { Bootstrap } from '../../Bootstrap';
import { DEFAULT_TASKS } from "./data";

export class TaskRepository implements ITaskRepository{

    constructor(private Task: Model<ITask>){}

    public async getAll(): Promise<ITask[]>{
        try{
            return await this.Task.find();
        }
        catch(error){
            Bootstrap.logger.log("error","Error in TaskRepository.getAll " + error);
            return undefined;
        }
    }

    public getDefaultTasks(): ITask[]{
        try{
            return DEFAULT_TASKS.map((task) => {
                let t: ITask = new this.Task();
                t.name = task;
                return t;
            });
        }
        catch(error){
            Bootstrap.logger.log("error","Error in TaskRepository.getDefaultTasks " + error);
            return undefined;
        }
    }

    public async getTask(id: String): Promise<ITask>{
        try{
            return await this.Task.findById(id);
        }
        catch(error){
            Bootstrap.logger.log("error","Error in TaskRepository.getTask " + error);
            throw new Error();
        }
    }

    public async addTask(Task: ITask): Promise<ITask>{
        try {
            return await Task.save();
        } catch (error) {
            Bootstrap.logger.log("error","Error in TaskRepository.addTask " + error);
            throw new Error();
        }        
    }

    public async updateTask(id: string, newTask: ITask): Promise<ITask>{
        try {
            const Task = await this.Task.findById(id);
            Task.name = newTask.name;
            return await Task.save();
        } catch (error) {
            Bootstrap.logger.log("error","Error in TaskRepository.updateTask " + error);
            throw new Error();
        }
    }

    public async deleteTask(id: string): Promise<boolean>{
        try {
            return (await this.Task.deleteOne({_id: id})).n === 1;
        } catch (error) {
            Bootstrap.logger.log("error","Error in TaskRepository.deleteTask " + error);
            throw new Error();
        }
    }
}