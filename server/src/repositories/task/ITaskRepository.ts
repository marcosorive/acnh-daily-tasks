import {ITask} from '../../interfaces/ITask';

export interface ITaskRepository {
    getAll(): Promise<ITask[]>;
    getDefaultTasks(): ITask[];
    getTask(id: String): Promise<ITask>;
    addTask(product: ITask): Promise<ITask>;
    updateTask(id: string, product: ITask): Promise<ITask>;
    deleteTask(id: string): Promise<boolean>;
}