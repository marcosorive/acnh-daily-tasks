import { Request, Response } from 'express';
import { ITask } from '../interfaces/ITask';
import { Bootstrap } from '../Bootstrap';
import { IUser } from 'interfaces/IUser';

export class TaskController{

    public static async getAllTasks(req: Request, res: Response): Promise<void> {
        try{
            if(req.cookies.user) {
                const tasks: ITask[] = await Bootstrap.userService.getAllTaskFromUser(req.cookies.user);
                res.status(200).send(tasks);
            }else{
                console.log("New cookie")
                const user: IUser = await Bootstrap.userService.createUser();
                res.cookie("user",user.cookieId);
                res.status(200).send(user.taskList);
            }            
        }
        catch(error){
            Bootstrap.logger.log('error','Error in TaskController.getAllTasks ' + error);
            res.status(500).send();
        }
    }

    // Not used right now
    public static async getTask (req: Request, res: Response): Promise<void>{
        try {
            const task: ITask = await Bootstrap.taskService.getTask(req.params.id);
            if(task === undefined){
                res.status(404).send();
            }else{
                res.status(200).send(task);
            }            
        } catch (error) {
            Bootstrap.logger.log('error','Error in TaskController.getTask ' + error);
            res.status(500).send();
        }
    }

    public static async createTask (req: Request, res: Response): Promise<void>{
        try{
            const task: ITask = await Bootstrap.userService.createTask(req.cookies.user, req.body.name);
            res.status(200).send(task);
        }
        catch(error){
            Bootstrap.logger.log('error','Error in TaskController.addTask: ' + error);
            res.status(500).send({error: error})
        }
    }

    public static async updateTask (req: Request, res: Response): Promise<void>{
        try{
            const task: ITask = await Bootstrap.userService.updateTask(req.cookies.user, req.body._id, req.body.name, req.body.isDone);
            if(task){
                res.status(200).send(task);
            }else{
                res.status(404).send();
            }
        }
        catch(error){
            Bootstrap.logger.log('error','Error in TaskController.updateTask: ' + error);
            res.status(500).send()
        }
    }

    public static async deleteTask (req: Request, res: Response): Promise<void>{
        try{
            if(await Bootstrap.userService.deleteTask(req.cookies.user, req.params.taskId)){
                res.status(200).send();
            }
            else{
                res.status(404).send();
            }
        }
        catch(error){
            Bootstrap.logger.log('error','Error in TaskController.deleteTask: ' + error);
            res.status(500).send({error: error})
        }
    }

    public static async markAllUndone (req: Request, res: Response): Promise<void>{
        try{            
            res.status(200).send(await Bootstrap.userService.markAllTasksUndone(req.cookies.user));
        }
        catch(error){
            Bootstrap.logger.log('error','Error in TaskController.deleteTask: ' + error);
            res.status(500).send({error: error})
        }
    }
}