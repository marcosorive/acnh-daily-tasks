import { Router } from "express";
import { TaskController } from "../controllers/TaskController";
import { AuthService } from "../services/AuthService";

export class TaskRoutes{

    constructor(private authService: AuthService, private taskController: TaskController){}

    public getTaskRoutes(): Router{
        const router: Router = Router();
        // GetAll
        router.get('', TaskController.getAllTasks);
        // Add 1 new task
        router.post('', this.authService.isAuthorized, TaskController.createTask);
        // Delte 1 task
        router.delete('/:taskId', this.authService.isAuthorized, TaskController.deleteTask);
        // Mark all as undone
        router.post('/allUndone', this.authService.isAuthorized, TaskController.markAllUndone);
        // Mark one as done
        router.put('/', this.authService.isAuthorized, TaskController.updateTask);
        return router;
    }

}

