"use strict";
import * as express from "express";
import {Request, Response } from 'express';
import * as cors from 'cors';
import * as mongoose from "mongoose";
import * as keys from "./config/keys";
import * as cookieParser from "cookie-parser";
import { Bootstrap } from "./Bootstrap";
import * as path from 'path';
var sslRedirect = require('heroku-ssl-redirect');

export class App {

    private readonly app: express.Application;
    private static readonly db: string = keys["mongoURI"];
    private readonly dev_server_url: string[] = ["http://localhost:4200", "https://acdailytasks.herokuapp.com"];
    private readonly corsOptions: cors.CorsOptions = {
        allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept", "X-Access-Token"],
        credentials: true,
        methods: "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
        origin: this.dev_server_url,
        preflightContinue: false
      };

    constructor(private readonly port: number) {
        this.port = port;
        this.app = express();
    }

    public startApp(): void {
        //This process.env.PORT is so the app works correctly in Heroku.
        //That reads the port from enviorment variables insetead of the one hardcorded here.
        this.app.listen(process.env.PORT || this.port, () => {
            new Bootstrap().setup();
            this.config();            
        });
        console.log("Server listening!");
    }

    private config(): void {
        this.app.use(cors(this.corsOptions));
        this.app.use(express.json());
        this.app.use(cookieParser());
        this.app.use(sslRedirect());
        // Setting public folder
        this.app.use(express.static(__dirname+'/public'));
        // Setting routes
        this.app.use('/api/task', Bootstrap.taskRoutes.getTaskRoutes());

        //  Uncoment this if you want to serve a frontend.
        // Serving frontend 
        this.app.use("*",(req: Request, res: Response) => {
            res.sendFile(path.join(__dirname+'/public/index.html'));
         })

        // Connecting DB
        mongoose.connect(App.db, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true })
            .then(() => console.log("MongoDB Connected!"))
            .catch((error) => console.log("Error connecting MongoDB: " + error));
    }
}