"use strict";
import * as mongoose from 'mongoose';
import { ITask } from "./ITask";

export interface IUser extends mongoose.Document {
    cookieId: String;
    taskList: ITask[];
};