"use strict";
import * as mongoose from 'mongoose';

export interface ITask extends mongoose.Document {
    name: String;
    isDone: Boolean;
};