import * as mongoose from 'mongoose';
import { IUser } from '../interfaces/IUser'
import { TaskSchema } from "../models/Task";

const userSchema = new mongoose.Schema({
    cookieId: { type: String, required: true },
    taskList: { type: [TaskSchema], required: false }
});

export const User = mongoose.model<IUser>("User", userSchema);