import * as mongoose from 'mongoose';
import { ITask } from '../interfaces/ITask'

export const TaskSchema = new mongoose.Schema({
    name: { type: String, required: true },
    isDone: { type: Boolean, default: false },
});

export const Task = mongoose.model<ITask>("Task", TaskSchema);