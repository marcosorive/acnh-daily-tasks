# Animal Crossing New Horizons Daily Tasks

## About

Web application that shows a list of daily tasks that can be done in Animal Crossing New Horizons. 
Every new user gets a default list of daily tasks. After that, every change will be saved.
This site uses cookies to remember the state of you task list. If you use another browser or device the state won't be saved across them. I'm working on improving this.

You can try it here: [https://acdailytasks.herokuapp.com/](https://acdailytasks.herokuapp.com/)

## Contact & Credit

For any inquiries please contact Marcos via twitter: [https://twitter.com/marcosorive](https://twitter.com/marcosorive)

The beautiful pattern in the background is a design by María Prego: [https://www.instagram.com/doots.design](https://www.instagram.com/doots.design)

## Screenshoots

![acnh-daily-tasks-screenshot](./acnh-daily-tasks-screenshot.png)

