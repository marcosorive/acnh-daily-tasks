import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Task } from "../core/task/task";
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private readonly lists_endpoint: string = "/api/task";
  private readonly undone_route: string = "/allUndone";

  constructor(private http: HttpClient) { }

  public getAllTasks(): Observable<Task[]>{
    return this.http.get<Task[]>(this.lists_endpoint, { withCredentials: true }).pipe(
      tap(),
      catchError(this.handleError)
    );
  }

  public updateTask(newTask: Task): Observable<Task>{
    return this.http.put<Task>(
      this.lists_endpoint, 
      newTask,
      { withCredentials: true }).pipe(
        tap(),
        catchError(this.handleError)
    );
  }

  public createTask(task: Task): Observable<Task>{
    return this.http.post<Task>(
      this.lists_endpoint, 
      task,
      { withCredentials: true }).pipe(
        tap(),
        catchError(this.handleError)
    );
  }

  public deleteTask(task: Task): Observable<void>{
    return this.http.delete<void>(
      this.lists_endpoint.concat("/").concat(task._id), 
      { withCredentials: true }).pipe(
      tap(),
      catchError(this.handleError)
    );
  }

  public markAllTasksUndone(): Observable<Task[]>{
    return this.http.post<Task[]>(
      this.lists_endpoint.concat(this.undone_route),
        {},
      { withCredentials: true }).pipe(
        tap(),
        catchError(this.handleError)
    )
  }

  private handleError(err: HttpErrorResponse) {
 
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {

        errorMessage = `An error occurred: ${err.error.message}`;
    } else {

        errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
    }
    console.error(errorMessage);
    return throwError(errorMessage);
}

}
