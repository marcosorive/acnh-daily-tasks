import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Task } from '../../core/task/task';
import { ApiService } from "../../services/api.service";

@Component({
  selector: 'task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit {

  @Input() public task: Task;
  @Input() public showDeleteButton: boolean;
  @Output() private notifyDelete: EventEmitter<string>;

  private deleteButtonClicked: boolean;

  constructor(private apiService: ApiService) { 
    this.notifyDelete = new EventEmitter<string>();
    this.deleteButtonClicked = false;
  }

  ngOnInit(): void {
  }

  public updateTaskState(): void {
    const toUpdateTask: Task = {
      _id: this.task._id,
      isDone: !this.task.isDone,
      name: this.task.name
    };
    console.log(toUpdateTask);
    this.apiService.updateTask(toUpdateTask).subscribe(
      (task) => {
        console.log(task);
        this.task = task;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  public get isTaskDone(){
    return this.task.isDone;
  }

  public deleteTask(){
    this.apiService.deleteTask(this.task).subscribe(
      () => {
        this.notifyDelete.emit(this.task._id);
      },
      (error) => {
        console.log(error);
      }
    )
  }
}
