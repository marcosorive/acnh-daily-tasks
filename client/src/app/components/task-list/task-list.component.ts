import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ApiService } from "../../services/api.service";
import { Task } from "../../core/task/task";

@Component({
  selector: 'task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent implements OnInit {

  public tasks: Task[];
  public newNameFormControl: FormControl;
  public showDeleteButton: boolean;
  
  private isAddButtonClicked: boolean;

  constructor(private apiservice: ApiService) { 
    this.tasks = [];
    this.newNameFormControl = new FormControl("");
    this.showDeleteButton = false;
    this.isAddButtonClicked = false;
  }

  ngOnInit(): void {
    this.apiservice.getAllTasks().subscribe(
      (tasks) => {
        this.tasks = tasks;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  public handleSubmitNewTask(): void{
    const newTask = {
      name: this.newNameFormControl.value,
    } as Task;
    this.apiservice.createTask(newTask).subscribe(
      (task) => {
        this.tasks.push(task)
        this.newNameFormControl.setValue("");
      },
      (error) => {
        console.log(error)
      }
    );
  }

  public handleToggleDeleteButton(): void{
    this.showDeleteButton = !this.showDeleteButton;
  }

  public handleDeletedTask(deletedTaskId: string): void{
    this.tasks = this.tasks.filter( (task) => {
      return task._id !== deletedTaskId;
    })
  }

  public handleAllTaskUndone(): void{
    this.apiservice.markAllTasksUndone().subscribe(
      (tasks) => {
        this.tasks = tasks;
      },
      (error) => {
        console.log(error);
      }
    )
  }
}
