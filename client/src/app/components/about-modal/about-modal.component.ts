import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'about-modal',
  templateUrl: './about-modal.component.html',
  styleUrls: ['./about-modal.component.scss']
})
export class AboutModalComponent implements OnInit {

  private _isOpen: boolean;

  constructor() { 
    this._isOpen = false;
  }

  ngOnInit(): void {
  }

  public toggleModal(){
    this._isOpen = !this._isOpen;
  }

  public get isOpen(){
    return this._isOpen;
  }

}
