import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cookie-consent',
  templateUrl: './cookie-consent.component.html',
  styleUrls: ['./cookie-consent.component.scss']
})
export class CookieConsentComponent implements OnInit {

  private readonly COOKIE_CONSENT: string = "cookie-consent";
  private readonly CONSENT_DAYS: number = 365;
  private _isConsented: boolean = false;

  constructor() {
      this._isConsented = this.getCookie(this.COOKIE_CONSENT) === '1';
  }

  ngOnInit(): void{

  }

  public consent(): void {
    this.setCookie(this.COOKIE_CONSENT, '1', this.CONSENT_DAYS);
    this._isConsented = true;
  }

  public get isConsented(): boolean{
    return this._isConsented;
  }

  private getCookie(name: string): string {
      let ca: Array<string> = document.cookie.split(';');
      let caLen: number = ca.length;
      let cookieName = `${name}=`;
      let c: string;

      for (let i: number = 0; i < caLen; i += 1) {
          c = ca[i].replace(/^\s+/g, '');
          if (c.indexOf(cookieName) == 0) {
              return c.substring(cookieName.length, c.length);
          }
      }
      return '';
  }

  private setCookie(name: string, value: string, expireDays: number, path: string = ''): void {
      let d:Date = new Date();
      d.setTime(d.getTime() + expireDays * 24 * 60 * 60 * 1000);
      let expires:string = `expires=${d.toUTCString()}`;
      let cpath:string = path ? `; path=${path}` : '';
      document.cookie = `${name}=${value}; ${expires}${cpath}`;
  }



}
