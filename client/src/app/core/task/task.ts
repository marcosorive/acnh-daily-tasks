export class Task {
    public _id: string;
    public name: string;
    public isDone: boolean;

    constructor (id: string, name: string, done: boolean){
        this._id = id;
        this.name = name;
        this.isDone = done;
    }
}
